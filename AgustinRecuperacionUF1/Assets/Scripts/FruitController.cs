using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class FruitController : MonoBehaviour
{
    [SerializeField] FruitStats fruitStats;
    [SerializeField] UnityEvent<int> eventToLaunchOnPicked;
    [SerializeField] Animator animator;
    bool picked;

    

    // Start is called before the first frame update
    void Start()
    {
        animator.Play(fruitStats.fruitName);
        eventToLaunchOnPicked.AddListener(GameManager.instance.AddScorePoints);
    }
   

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player" && !picked) StartCoroutine(PickedCoroutine());
    }

    IEnumerator PickedCoroutine()
    {
        eventToLaunchOnPicked.Invoke(fruitStats.pointsGiven);
        picked = true;
        animator.Play("Picked");
        yield return new WaitForSeconds(1.5f);
        Destroy(this.gameObject);
    }

}
