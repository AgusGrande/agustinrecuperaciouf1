using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    int currentScore = 0;
    float countdownTimer = 25f;
    bool gameOver = false;
    bool timerGoing = false;

    [SerializeField] TMPro.TextMeshProUGUI currentScoreText;
    [SerializeField] TMPro.TextMeshProUGUI currentTimeText;
    
    [SerializeField] GameObject gameOverScreen;
    [SerializeField] TMPro.TextMeshProUGUI yourScoreText;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else Destroy(gameObject);   
    }

    // called first
    void OnEnable()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    // called second
    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        Debug.Log("OnSceneLoaded: " + scene.name);
        Debug.Log(mode);
        string currentSceneName = SceneManager.GetActiveScene().name;

        if (currentSceneName == "Menu")
        {
            currentScoreText.gameObject.SetActive(false);
            currentTimeText.gameObject.SetActive(false);
        }
        else
        {
            currentScoreText.gameObject.SetActive(true);
            currentTimeText.gameObject.SetActive(true);
        }
    }

    public void StartTouched()
    {
        timerGoing = true;
    }

    public void FinishTouched()
    {
        timerGoing = false;
        LoadNextLevel();
    }
    public void LoadNextLevel()
    {
        string currentSceneName = SceneManager.GetActiveScene().name;

        if (currentSceneName == "Level1") SceneManager.LoadScene("Level2");

        else if (currentSceneName == "Level2") SceneManager.LoadScene("Level1");

    }
    


    // What to do on updating Score
    public void AddScorePoints(int pointsToAdd)
    {
        currentScore += pointsToAdd;
        currentScoreText.text = "SCORE: "+currentScore;
    }

    public void ToRestart()
    {
        gameOverScreen.SetActive(false);
        currentScore = 0;
        timerGoing = false;
        gameOver = false;
        countdownTimer = 25f;
        currentTimeText.text = "TIME: " + Mathf.CeilToInt(countdownTimer) + "s";
        currentScoreText.text = "SCORE: " + currentScore;
        SceneManager.LoadScene("Level1");
    }

    private void Update()
    {
        if (countdownTimer > 0 && timerGoing && !gameOver) UpdateTimerUI();
        else if(!gameOver && countdownTimer <= 0)TimerEnded();
    }

    void TimerEnded()
    {
        gameOver = true;
        gameOverScreen.SetActive(true);
        yourScoreText.text = "YOUR SCORE: " + currentScore;
    }

    private void UpdateTimerUI()
    {
        countdownTimer -= Time.deltaTime;
        currentTimeText.text = "TIME: "+ Mathf.CeilToInt(countdownTimer)+"s";
    }
}
