using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallingPlatformBehaviour : MonoBehaviour
{
    [SerializeField]Rigidbody2D rb;
    [SerializeField] float heightVariation = 0.35f;
    [SerializeField] float speed = 0.3f;
    [SerializeField] float fallingSpeed = 10f;
    [SerializeField] float timeToStartFalling = 0.5f;
    [SerializeField] ParticleSystem particles;
    bool falling,goingUp;
    [SerializeField] Animator animator;

    public Vector2 startPosition;

    private void Awake()
    {
        startPosition = new Vector2(transform.position.x, transform.position.y);
        rb.velocity = new Vector2(0, -speed);
    }

    private void Update()
    {
        if (!falling)
        {
            // Changing current direction if surpassed the limit
            if ((goingUp && startPosition.y + heightVariation < transform.position.y) 
                || (!goingUp && startPosition.y - heightVariation > transform.position.y))
                ChangeDirection();                
        }
    }
    // Changing the current direction of the platform to the opposite
    void ChangeDirection()
    {
        rb.velocity *= -1f;
        goingUp = !goingUp;
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        // Getting GroundedControl position to only deactivate if the base is higher
        if (!falling)
            if(collision.gameObject.GetComponentInChildren<GroundedControl>().transform.position.y>transform.position.y)
                StartCoroutine("startFallingCoroutine");
    }
    IEnumerator startFallingCoroutine()
    {
        particles.Stop();
        animator.Play("FallingPlatformOff");
        rb.velocity = Vector2.zero;
        falling = true;
        yield return new WaitForSeconds(timeToStartFalling);
        particles.gameObject.SetActive(false);
        rb.velocity = new Vector2(0,-fallingSpeed);
        yield return new WaitForSeconds(3f);
        Destroy(this.gameObject);

    }

}
