using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RanitaBehaviour : MonoBehaviour
{
    Rigidbody2D rb;
    Animator animator;

    [SerializeField] PlayerStats playerStats;
    [Tooltip("Updated on child <GroundedControl>")]public bool isGrounded;
    float jumpTime;
    bool jumping,isFlipped;
    float inputHorizontal;

    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
    }


    private void Update()
    {
        inputHorizontal = Input.GetAxisRaw("Horizontal");
        rb.velocity = new Vector2(inputHorizontal * playerStats.moveSpeed, rb.velocity.y);


        // SHOULD FLIP?? CONTROL
        if ((isFlipped && inputHorizontal > 0f) || (!isFlipped && inputHorizontal < 0f))
        {
            isFlipped = !isFlipped;
            transform.localScale = new Vector3(-1f*transform.localScale.x, transform.localScale.y, transform.localScale.z);
        }
        // JUMP CONTROL
        if (Input.GetKeyDown(KeyCode.W) && isGrounded)
        {
            jumping = true;
            jumpTime = 0;
            animator.Play("Jump");
        }
        if (jumping)
        {
            rb.velocity = new Vector2(rb.velocity.x, playerStats.jumpAmount);
            jumpTime += Time.deltaTime;
        }
        if (Input.GetKeyUp(KeyCode.W) || jumpTime > playerStats.maxJumpButtonTime)
        {
            jumping = false;
        }

        // OTHER ANIMATIONS CONTROL
        if (rb.velocity.y < 0 && !isGrounded) animator.Play("Fall");
        else if (rb.velocity == Vector2.zero && isGrounded) animator.Play("Idle");
        else if (rb.velocity.y<0.1f && isGrounded) animator.Play("Run"); // Using 0.1f to avoid staying on falling running on a wall
    }

   

}
