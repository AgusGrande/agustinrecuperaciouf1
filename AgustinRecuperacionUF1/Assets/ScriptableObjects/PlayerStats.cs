using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "My Assets/Player Stats")]
public class PlayerStats : ScriptableObject
{
    [Header("Player Stats")]
    public float MaxJumpButtonTime = 0.3f;
    public float JumpAmount = 6;
    public float MoveSpeed = 4;

    public float maxJumpButtonTime { get => MaxJumpButtonTime; }
    public float jumpAmount { get => JumpAmount; }
    public float moveSpeed { get => MoveSpeed; }


}
