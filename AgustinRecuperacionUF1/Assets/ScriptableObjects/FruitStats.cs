using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "My Assets/FruitStats")]
public class FruitStats : ScriptableObject
{
    [SerializeField] int PointsGiven;
    [Tooltip("Used for animation")] [SerializeField] string FruitName;

    public int pointsGiven { get => PointsGiven; }
    public string fruitName { get => FruitName; }
}
