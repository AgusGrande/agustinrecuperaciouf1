using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundedControl : MonoBehaviour
{
    RanitaBehaviour ranitaBehaviour;
    private void Awake()
    {
        ranitaBehaviour = GetComponentInParent<RanitaBehaviour>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "map") ranitaBehaviour.isGrounded = true;
    }
    private void OnTriggerStay2D(Collider2D collision)
    {
        if(collision.tag == "map" && ranitaBehaviour.isGrounded==false) ranitaBehaviour.isGrounded = true;
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "map") ranitaBehaviour.isGrounded = false;
    }

}
