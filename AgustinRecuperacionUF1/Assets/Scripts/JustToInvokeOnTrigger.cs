using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JustToInvokeOnTrigger : MonoBehaviour
{
    [SerializeField] GameEvent eventToRaise;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        eventToRaise.Raise();
    }
}
